from string import Template

configuration = {}
module_name='k8s'
commands = ['create']
def execute(command, params, app_config):
    global configuration
    configuration = app_config
    if command == 'create':
        k8s_create(params)
def k8s_create(params):
    subcommand = params[0] if len(params) > 0 else None
    remaining_params = params[1:] if len(params) > 1 else None
    if subcommand:
        name = '-'.join(remaining_params)
        if subcommand in ['deploy', 'deployment']:
            content = deployment.substitute(name=name, namespace='default')
            filename = f'{name}-deploy.yaml'
        elif subcommand in ['service', 'svc']:
            content = service.substitute(name=name, namespace='default')
            filename = f'{name}-svc.yaml'
        elif subcommand in ['ingress', 'ing']:
            content = ingress.substitute(name=name, namespace='default')
            filename = f'{name}-ingress.yaml'
        elif subcommand in ['storageclass', 'sc']:
            content = storageclass.substitute(name=name, namespace='default')
            filename = f'{name}-sc.yaml'
        elif subcommand in ['volumeclaim', 'pvc']:
            content = volumeclaim.substitute(name=name, namespace='default')
            filename = f'{name}-pvc.yaml'

        elif subcommand in ['all']:
            types = {'ingress': ingress, 'sc': storageclass, 'pvc': volumeclaim, 'deploy': deployment, 'svc': service}
            for objecttype in types:
                template = types.get(objecttype)
                content = template.substitute(name=name, namespace='default')
                filename = f'{name}-{objecttype}.yaml'
                with open(filename, 'w') as writer:
                    writer.write(content)
                print(f'Manifest file {filename} written.')
            exit()
        else:
            print('Unknown type')
            exit()
        with open(filename, 'w') as writer:
            writer.write(content)
        print(f'Manifest file {filename} written.')


ingress=Template("""apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  annotations:
    certmanager.k8s.io/cluster-issuer: letsencrypt-prod
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /
  name: $name-ing
  namespace: $namespace
spec:
  rules:
  - host: example.com
    http:
      paths:
      - backend:
          serviceName: $name
          servicePort: 80
  tls:
  - hosts:
    - example.com
    secretName: $name-cert
""")
volumeclaim=Template("""apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: $name-pvc
  namespace: $namespace
spec:
  accessModes:
    - ReadWriteMany
  dataSource: null
  resources:
    requests:
      storage: 2Gi
  storageClassName: $name-sc
""")
storageclass=Template("""apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: $name-sc
parameters:
  skuName: Standard_LRS
provisioner: kubernetes.io/azure-file
reclaimPolicy: Delete
volumeBindingMode: Immediate
""")
service=Template("""apiVersion: v1
kind: Service
metadata:
  namespace: $namespace
  name: $name-svc
  labels:
    name: $name
spec:
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
    app: $name
""")
deployment=Template("""apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
  labels:
    app: $name
  name: $name
  namespace: $namespace
spec:
  selector:
    matchLabels:
      app: $namespace
  template:
    metadata:
      labels:
        app: $name
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: $name
        ports:
        - containerPort: 80
          protocol: TCP
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 10m
            memory: 32Mi
        volumeMounts:
          - mountPath: /mnt/default-volume
            name: default-volume
      restartPolicy: Always
      volumes:
        - name: default-volume
          persistentVolumeClaim:
            claimName: $name-pvc
""")
